# Filler module for Drupal

Functions for generating filler content.

## Acknowledgements

* The lipsum.txt wordlist was adapted from lipsum.com. (@TODO Verify license)

All included images are in the public domain.
